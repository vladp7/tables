//
//  Project.swift
//  Tables
//
//  Created by Владислав on 10.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

import Foundation

class Project {
    var name = ""
    var version = ""
    var time = ""
    var url = ""
    
    init(name: String, version: String, time: String, url: String) {
        self.name = name
        self.version = version
        self.time = time
        self.url = url
    }
}
