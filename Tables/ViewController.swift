//
//  ViewController.swift
//  Tables
//
//  Created by Владислав on 08.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    @IBOutlet weak var Open: UIBarButtonItem!
    
    var projects = [Project]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        get_data_from_url("http://bnet.i-partner.ru/projects/playMarket/?json=1")
        
        Open.target = self.revealViewController()
        Open.action = Selector("revealToggle:")
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        
        cell.thumbnailImageView?.image = UIImage(named: "Swift")
        cell.nameLabel?.text = projects[indexPath.row].name
        cell.versionLabel?.text = projects[indexPath.row].version
        cell.timeLabel?.text = projects[indexPath.row].time
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let optionMenu = UIAlertController(title: "Действие", message: "Что сделать?", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        let defaultActionHandler = {(action: UIAlertAction!) -> Void in
            let alertMessage = UIAlertController(title: "Сервис недоступен", message: "Просите, но сейчас выполнить команду невозможно.", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
        }
        
        let openURLActionHandler = {(action:UIAlertAction!) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: self.projects[indexPath.row].url)!)
            } else {
                UIApplication.shared.openURL(URL(string: self.projects[indexPath.row].url)!)
            }
        }
        
        let startAction = UIAlertAction(title: "Запустить", style: .default, handler: defaultActionHandler)
        let downloadAction = UIAlertAction(title: "Скачать", style: .default, handler: openURLActionHandler)
        
        optionMenu.addAction(cancelAction)
        optionMenu.addAction(startAction)
        optionMenu.addAction(downloadAction)
        
        self.present(optionMenu, animated: true, completion: nil)
        
        //убрать выделение ячейки
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    /*override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            nameArray.remove(at: indexPath.row)
            versionArray.remove(at: indexPath.row)
            timeArray.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }*/
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default, title: "Удалить", handler: {(action, indexPath) -> Void in
            self.projects.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        })
        return [deleteAction]
    }
    
    func get_data_from_url(_ link:String) {
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                return
            }
            self.extract_json(data!)
        })
        
        task.resume()
        
    }
    
    func extract_json(_ data: Data) {
        
        
        let json: Any?
        
        do {
            json = try JSONSerialization.jsonObject(with: data, options: [])
        }
        catch {
            return
        }
        
        guard let data_list = json as? NSArray else {
            return
        }
        
        func formDate(timeInterval: TimeInterval) -> String {
            let date = Date(timeIntervalSince1970: timeInterval)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let formated = formatter.string(from: date)
            return formated
        }
        
        if let list = json as? NSArray {
            for i in 0 ..< data_list.count {
                if let obj = list[i] as? NSDictionary {
                    if let name = obj["name"] as? String {
                        if let version = obj["version"] as? String {
                            if let time = obj["time"] as? TimeInterval{
                                if let url = obj["url"] as? String {
                                    //projects.append(Project(name: name, version: version, time: String(describing: Date(timeIntervalSince1970: time)), url: url))
                                    let formatedDate = formDate(timeInterval: time)
                                    projects.append(Project(name: name, version: version, time: formatedDate, url: url))
                                }
                            }
                        }
                    }
                }
            }
        }
        
        DispatchQueue.main.async(execute: {self.do_table_refresh()})
        projects = projects.sorted {$0.time > $1.time}
        
    }
    
    func do_table_refresh() {
        self.tableView.reloadData()
    }

}
